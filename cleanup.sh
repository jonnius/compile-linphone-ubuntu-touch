#!/bin/bash

OUTPUT=`pwd`/output

set -x
set -e

rm -r $OUTPUT/include
rm -r $OUTPUT/share/doc
rm -r $OUTPUT/share/aclocal
rm -r $OUTPUT/share/BCUnit

rm $OUTPUT/bin/linphone-daemon-pipetest

find $OUTPUT -name *.pdf -delete
find $OUTPUT -name cmake -exec rm -r {} +
find $OUTPUT -name pkgconfig -exec rm -r {} +
find $OUTPUT -name *.a -delete

find $OUTPUT -type d -empty -delete
